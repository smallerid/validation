<?php

declare(strict_types=1);

namespace Purwantoid\Validation;

use Respect\Validation\Exceptions\ComponentException;
use Respect\Validation\Exceptions\ValidationException;
use Respect\Validation\Rules\AllOf;

/**/

use Respect\Validation\Factory as FactoryRespect;

class Validator extends AllOf
{

  /**
   * Creates a new Validator instance with a rule that was called on the static method.
   *
   * @param array $arguments
   *
   * @throws ComponentException
   */
  public static function __callStatic(string $ruleName, array $arguments): \Respect\Validation\Validator
  {
    return \Respect\Validation\Validator::create()->__call($ruleName, $arguments);
  }

  /**
   * {@inheritDoc}
   */
  public function check($input): void
  {
    try
    {
      parent::check($input);
    }
    catch (ValidationException $exception)
    {
      if (\count($this->getRules()) === 1 && $this->template)
      {
        $exception->setTemplate($this->template);
      }

      throw $exception;
    }
  }

  /**
   * Create a new rule by the name of the method and adds the rule to the chain.
   *
   * @param array $arguments
   *
   * @throws ComponentException
   */
  public function __call(string $ruleName, array $arguments): self
  {
    foreach (Factory::getDefaultInstance()->getRulesNamespaces() as $ruleNameSpace)
    {
      $this->addRule((new FactoryRespect())->rule($ruleNameSpace, $arguments));
    }
    return $this;
  }

}