<?php

declare(strict_types=1);

namespace Purwantoid\Validation;

class Factory
{
  private static Factory $defaultInstance;
  /**
   * @var string[]
   */
  private $rulesNamespaces = [];

  public static function getDefaultInstance(): self
  {
    if (self::$defaultInstance === null)
    {
      self::$defaultInstance = new self();
    }

    return self::$defaultInstance;
  }

  /**
   * Define the default instance of the Factory.
   */
  public static function setDefaultInstance(self $defaultInstance): void
  {
    self::$defaultInstance = $defaultInstance;
  }

  public function withRuleNamespace(string $rulesNamespace): self
  {
    $clone = clone $this;
    $clone->rulesNamespaces[] = trim($rulesNamespace, '\\');

    return $clone;
  }

  public function getRulesNamespaces(): array
  {
    return $this->rulesNamespaces;
  }
}